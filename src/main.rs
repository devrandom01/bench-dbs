#![allow(unused)]

use std::path::Path;
use std::time::Instant;

#[cfg(feature = "redb")]
fn bench_redb(path: &str, b: &mut Bencher) {
    use redb::{TableDefinition, Database};
    const TABLE: TableDefinition<u32, u32> = TableDefinition::new("t1");
    let path = Path::new(path).join("redb");
    let db = Database::create(path).unwrap();
    let mut i: u32 = 0;
    b.iter(|| {
        let tx = db.begin_write().unwrap();
        {
            let mut table = tx.open_table(TABLE).unwrap();
            table.insert(&i, &i);
        }
        tx.commit().unwrap();
        i += 1;
    });
}

#[cfg(feature = "sled")]
fn bench_sled(path: &str, b: &mut Bencher) {
    let db = sled::open(path).unwrap();
    db.clear().unwrap();
    db.flush().unwrap();
    let mut i: u32 = 0;
    b.iter(|| {
        db.insert(i.to_be_bytes().to_vec(), &i.to_be_bytes()).unwrap();
        db.flush().unwrap();
        i += 1;
    });
}

#[cfg(feature = "rocksdb")]
fn bench_rocksdb(path: &str, b: &mut Bencher) {
    rocksdb::DB::destroy(&rocksdb::Options::default(), path).unwrap();
    let db = rocksdb::DB::open_default(path).unwrap();
    let mut i: u32 = 0;
    b.iter(|| {
        db.put(i.to_be_bytes(), &i.to_be_bytes()).unwrap();
        db.flush_wal(true).unwrap();
        i += 1;
    });
}

fn bench_empty(b: &mut Bencher) {
    b.iter(|| {})
}

struct Bencher(&'static str, u64);

impl Bencher {
    fn iter<F>(&mut self, mut f: F)
    where
        F: FnMut(),
    {
        // warmup
        for _ in 0..self.1 / 2 {
            f();
        }

        let start = Instant::now();
        for _ in 0..self.1 {
            f();
        }
        let elapsed_nanos = (Instant::now() - start).as_nanos() as u64;
        let iter_nanos = elapsed_nanos / self.1;
        println!("{:<8}: {:>6} us per iteration", self.0, iter_nanos / 1000);
    }
}

fn main() {
    // the first argument is an optional path to a directory to use for the benchmarks
    let path = std::env::args().nth(1).unwrap_or_else(|| "data".to_owned());
    #[cfg(feature = "redb")]
    bench_redb(&path, &mut Bencher("redb", 100));
    #[cfg(feature = "sled")]
    bench_sled(&path, &mut Bencher("sled", 100));
    #[cfg(feature = "rocksdb")]
    bench_rocksdb(&path, &mut Bencher("rocksdb", 100));
}
