# Benchmark various Rust local database crates

## Usage

Specify a directory on appropriate storage for the test (SSD / rotating).  Ensure that the target directory exists.

For example:

    cargo run --release -- ssd

## Results

On SSD:

    sled    :     71 us per iteration
    rocksdb :   9518 us per iteration
    redb    :   3956 us per iteration

On rotating disk:

    sled    :    182 us per iteration
    rocksdb :  24038 us per iteration
    redb    :  11483 us per iteration

## Discussion

Sled is not flushing to disk correctly, since it is physically impossible to sync in less than a few ms to a rotating disk.  Digging further, it looks like they use the `sync_file_range` syscall, which does not ensure the data is on disk.  The correct syscall is `fdatasync`, which is used by RocksDB.
